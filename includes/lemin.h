/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 17:34:41 by akazian           #+#    #+#             */
/*   Updated: 2014/03/23 19:24:40 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _LEMIN_H
# define _LEMIN_H

# include <string.h>
# include <libft.h>

typedef struct		s_graph
{
	char			*name;
	struct s_graph	**links;
	int				y;
	int				x;
	struct s_graph	*next;
	int				here;
}					t_graph;

typedef struct		s_env
{
	t_graph			*lk_lst;
	t_graph			*graph;
	t_graph			*start;
	t_graph			*end;
	t_list			**final_list;
	int				ants;
}					t_env;

t_graph	*new_graph_elem(void);
void	list_graph_elem(t_graph **g, int y, int x, char *name);
t_graph	*search_graph_elem(t_graph *g, char *name);
int		ft_graphlen(t_graph **src);
void	graph_array(t_graph *src, t_graph *add);
void	del_graph(t_graph *g);

int		ft_error(char *op, int error);
int		ft_lemin_lexer(t_env *e);
int		ft_lst_room(t_env *e, char *line, int *command);
int		ft_parse(t_env *e, char *line);
int		add_link(char *a, char *b, t_env *e);
int		ft_test_alnum(char *str, int end);
int		ft_resolv_lem(t_env *e);
void	add_elem_lst_tab(t_graph *g, t_env *e, int dell);
int		ft_len_list(t_list **src);
void	del_final_list(t_list **list);
int		ft_print_ants(t_env *e);

#endif
