# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: akazian <akazian@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 16:00:35 by akazian           #+#    #+#              #
#    Updated: 2014/03/23 19:03:03 by akazian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	lem-in
CC		=	gcc
CFLAGS		=	-Wall -Wextra -Werror -g3 -pedantic
LIBFTDIR	=	./libft/
LIBFTH		=	-I$(LIBFTDIR)
LIBFTFLAGS	=	-L$(LIBFTDIR) -lft
INCS_DIR	=	includes
OBJS_DIR	=	objects
SRCS_DIR	=	sources
SRCS		=	main.c\
			ft_graph.c\
			ft_graph_array.c\
			ft_lst_room.c\
			ft_parse_in_graph.c\
			ft_parser.c\
			ft_test_alnum.c\
			ft_resolv_lemin.c\
			ft_final_list.c\
			ft_print_ants.c

OBJS 			=	$(patsubst %.c, $(OBJS_DIR)/%.o, $(SRCS))

all				:	$(NAME)

$(NAME)			:	$(OBJS_DIR) $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LIBFTFLAGS)

$(OBJS_DIR)/%.o	:	$(addprefix $(SRCS_DIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $^ -I $(INCS_DIR) $(LIBFTH)

$(OBJS_DIR)	:	makelibft
	mkdir -p $(OBJS_DIR)

makelibft:
	make -C $(LIBFTDIR)

fclean			:	clean
	rm -f $(NAME)

clean			:
	rm -rf $(OBJS_DIR)

re				:	fclean all

.PHONY: clean all re fclean
