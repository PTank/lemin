/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_ants.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 18:59:22 by akazian           #+#    #+#             */
/*   Updated: 2014/03/23 20:24:17 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>

static void	ft_put_ants(t_list *list)
{
	ft_putchar('L');
	ft_putnbr(list->content_size);
	ft_putchar('-');
	ft_putstr(list->content);
	ft_putchar(' ');
}

static void	ft_echange_ants(t_env *e, int i)
{
	e->final_list[i]->content_size = e->final_list[i - 1]->content_size;
	e->final_list[i - 1]->content_size = 0;
	ft_put_ants(e->final_list[i]);
}

static int	ft_put_end(int ex, int ants)
{
	if (!ex)
		ft_putchar('\n');
	ants++;
	return (ants);
}

int			ft_print_ants(t_env *e)
{
	int	i;
	int ants;
	int	ex;

	ants = ex = i = 0;
	while (!ex)
	{
		i = ft_len_list(e->final_list) - 1;
		if (ants <= e->ants)
			e->final_list[0]->content_size = ants;
		while (i >= 0)
		{
			if (!e->final_list[i + 1] && e->final_list[i]->content_size)
			{
				if ((int) e->final_list[i]->content_size == e->ants && (ex = 1))
					break ;
				e->final_list[i]->content_size = 0;
			}
			if (i - 1 >= 0 && e->final_list[i - 1]->content_size)
				ft_echange_ants(e, i);
			i--;
		}
		ants = ft_put_end(ex, ants);
	}
	return (1);
}
