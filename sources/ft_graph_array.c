/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_graph_array.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 15:34:52 by akazian           #+#    #+#             */
/*   Updated: 2014/03/23 19:44:03 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>
#include <libft.h>
#include <stdlib.h>

int		ft_graphlen(t_graph **src)
{
	int	i;

	i = 0;
	if (src)
	{
		while (src[i] != NULL)
			i++;
	}
	return (i + 1);
}

void	graph_array(t_graph *src, t_graph *add)
{
	int		size;
	int		i;
	t_graph	**new_array;

	i = size = 0;
	if (!src || !add)
		return ;
	if (!src->links)
		size = 1;
	else
		size = ft_graphlen(src->links);
	if (!(new_array = (t_graph **)ft_memalloc(sizeof(t_graph *) * (size + 1))))
		return ;
	while (i < size)
	{
		if (src->links && src->links[i])
			new_array[i] = src->links[i];
		else if (i == size - 1)
			new_array[i] = add;
		i++;
	}
	new_array[i] = 0;
	if (src->links)
		free(src->links);
	src->links = new_array;
}
