/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 17:33:50 by akazian           #+#    #+#             */
/*   Updated: 2014/03/24 14:22:32 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>
#include <libft.h>

int		ft_error(char *op, int error)
{
	if (error == 0)
		return (error);
	ft_putstr_fd("ERROR", 2);
	if (op)
		ft_putendl_fd(op, 2);
	else
		ft_putchar_fd('\n', 2);
	return (error);
}

int		ft_init_env(t_env *e)
{
	e->lk_lst = NULL;
	e->start = NULL;
	e->end = NULL;
	e->ants = 0;
	e->final_list = NULL;
	return (1);
}

int		main(int argc, char **argv)
{
	t_env	e;
	char	*ants;
	int		error;

	error = 0;
	if (argc != 1 && (error = 1) && argv)
		return (ft_error(": Usage: ./lemin < arg", 1));
	while ((get_next_line(0, &ants)) > 0 && ants && ants[0] == '#')
		ft_strdel(&ants);
	ft_init_env(&e);
	if (ants)
		ft_putendl(ants);
	else
		error = 1;
	if (!error && (ft_test_alnum(ants, 0) || !(e.ants = ft_atoi(ants))))
		return (ft_error( NULL, 1));
	if ((error = ft_lemin_lexer(&e)))
		ft_error(NULL , error);
	if (error != 1)
		error = ft_resolv_lem(&e);
	if (error == 3)
		ft_error(": Can't find path", error);
	return (error);
}
