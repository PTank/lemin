/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_in_graph.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 12:03:27 by akazian           #+#    #+#             */
/*   Updated: 2014/03/17 17:27:16 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>
#include <libft.h>

int	add_link(char *a, char *b, t_env *e)
{
	t_graph	*a_node;
	t_graph	*b_node;

	if (!(a_node = search_graph_elem(e->lk_lst, a))
			|| !(b_node = search_graph_elem(e->lk_lst, b)))
		return (2);
	graph_array(a_node, b_node);
	graph_array(b_node, a_node);
	return (0);
}

int	ft_parse(t_env *e, char *line)
{
	int		i;
	int		j;
	char	*a_node;
	char	*b_node;

	if (!e->start || !e->end)
		return (2);
	j = i =  0;
	a_node = b_node = NULL;
	while (line[i])
	{
		if (line[i] == '-')
		{
			a_node = ft_strsub(line, i - j, j);
			j = 0;
			i++;
			continue ;
		}
		i++;
		j++;
	}
	if (j)
		b_node = ft_strsub(line, i - j, j);
	return (add_link(a_node, b_node, e));
}
