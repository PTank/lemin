/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_resolv_lemin.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 14:47:06 by akazian           #+#    #+#             */
/*   Updated: 2014/03/24 16:04:29 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>
#include <libft.h>

static int	ft_ret_n(int *j, int i, t_graph *room, t_env *e)
{
	if (room->links && room->links[i] == e->end)
		return (j[1] + 1);
	else if (j[0])
		return (j[1]);
	return (0);
}

static int	ft_check_end(t_env *e, t_graph *room, int h, int n)
{
	int	i;
	int	j[2] = {0};
	int	reth;

	i = 0;
	while (room->links && room->links[i] && room->links[i] != e->end)
	{
		if (h && !room->links[i]->here
			&& (reth = ft_check_end(e, room->links[i], h - 1, 1)))
		{
			if (j[1] < reth && (j[1] = reth))
				j[0] = i;
		}
		i++;
	}
	if (n && (j[1] = h + 1))
		return (ft_ret_n(j, i, room, e));
	else if (room->links && room->links[i] == e->end)
		return (i);
	else if (!n && j[0])
		return (j[0]);
	return (0);
}

static int	ft_path_finder(t_graph *tmp, t_env *e)
{
	int		w;
	int		i;

	i = w = 0;
	tmp->here = 1;
	add_elem_lst_tab(tmp, e, 0);
	if (tmp == e->end)
		return(0);
	if ((w = ft_check_end(e, tmp, 6, 0)))
		return (ft_path_finder(tmp->links[w], e));
	while (tmp->links[i])
	{
		while (tmp->links[i] != e->end && !tmp->links[i]->links)
			i++;
		if (tmp->links[i] && !tmp->links[i]->here
				&& !ft_path_finder(tmp->links[i], e))
			return (0);
		if (tmp->links[i])
			i++;
	}
	tmp->here = 0;
	add_elem_lst_tab(tmp, e, 1);
	return (1);
}

int			ft_resolv_lem(t_env *e)
{
	t_graph	*tmp;

	if (!e->start || !e->start->links || !e->end || !e->end->links)
		return (3);
	tmp = e->start;
	if (ft_path_finder(tmp, e) == 1)
		return (3);
	ft_print_ants(e);
	del_final_list(e->final_list);
	del_graph(e->lk_lst);
	return (0);
}
