/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_final_list.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 17:57:48 by akazian           #+#    #+#             */
/*   Updated: 2014/03/23 20:25:23 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>
#include <stdlib.h>

int		ft_len_list(t_list **src)
{
	int	i;

	i = 0;
	if (src)
	{
		while (src[i] != NULL)
			i++;
	}
	return (i);
}

void	add_elem_lst_tab(t_graph *g, t_env *e, int dell)
{
	int		size;
	t_list	**new;
	int		i;

	i = size = 0;
	if (!e->final_list)
		size = 1;
	else
		size = ft_len_list(e->final_list) + 1 - (dell * 2);
	if (!(new = (t_list **)ft_memalloc(sizeof(t_list *) * (size + 1))))
		return ;
	while (i < size)
	{
		if (e->final_list && e->final_list[i])
			new[i] = e->final_list[i];
		if (!dell && i == size - 1)
			new[i] = ft_lstnew(g->name, 0);
		i++;
	}
	if (dell)
		free(e->final_list[i]);
	if (e->final_list)
		free(e->final_list);
	e->final_list = new;
}

void	del_final_list(t_list **list)
{
	int	i;

	i = 0;
	while (list[i])
	{
		free(list[i]);
		i++;
	}
	free(list);
}
