/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_test_alnum.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 14:47:17 by akazian           #+#    #+#             */
/*   Updated: 2014/03/23 14:47:19 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <lemin.h>

int	ft_test_alnum(char *ants, int end)
{
	int	i;

	i = 0;
	if (!end)
		end = ft_strlen(ants);
	while (i != end && ft_isdigit(ants[i]))
		i++;
	if (ants[i] && !ft_isdigit(ants[i]))
		return (1);
	return (0);
}
