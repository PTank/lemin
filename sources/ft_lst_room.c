/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_room.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 15:58:46 by akazian           #+#    #+#             */
/*   Updated: 2014/03/23 13:57:47 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>
#include <libft.h>

static int	ft_save_pos(char *line, t_graph *tmp, int j, int pos)
{
	line[j - 1] = 0;
	if (ft_test_alnum(line, j))
		return (1);
	if (pos == 1)
		tmp->x = ft_atoi(line);
	if (pos == 2)
		tmp->y = ft_atoi(line);
	return (0);
}

static int	ft_add_tmp(char *line, t_graph *tmp, int j, int s)
{
	int	error;

	error = 0;
	if (s == 1)
		tmp->name = ft_strsub(line - 1, 0, j);
	else if (s == 2)
		error = ft_save_pos(line, tmp, j, 1);
	else if (s == 3)
		error = ft_save_pos(line, tmp, j, 2);
	return (error);
}

static int	ft_add_elem_value(char *line, t_graph *tmp)
{
	int	i;
	int	s;
	int	j;
	int	error;

	i = s = j = error = 0;
	while (line[i] && !error)
	{
		if (line[i] == ' ')
		{
			error = ft_add_tmp(line + i - j + 1, tmp, j, s);
			j = 0;
		}
		else if (line[i + 1] == ' ' || line[i + 1] == '\0')
			s++;
		if (s > 3)
			return (1);
		j++;
		i++;
	}
	if (!error)
		error = ft_add_tmp(line + i - j + 1, tmp, j, s);
	if (s != 3)
		error = 1;
	return (error);
}

int			ft_lst_room(t_env *e, char *line, int *command)
{
	t_graph	*new;

	new = new_graph_elem();
	if (*command == 1)
		e->start = new;
	else if (*command == 2)
		e->end = new;
	*command = 0;
	new->next = e->lk_lst;
	e->lk_lst = new;
	return (ft_add_elem_value(line, new));
}
