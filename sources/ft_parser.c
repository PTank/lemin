/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 17:39:49 by akazian           #+#    #+#             */
/*   Updated: 2014/03/23 20:28:51 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include <lemin.h>
#include <libft.h>

static int	ft_ret_link_value(char *line, int linker)
{
	int		i;

	i = 0;
	ft_putendl(line);
	while (linker == 0 && line[i] && line[i] != '-')
		i++;
	if (linker || line[i] == '-')
		return (1);
	return (0);
}


static int	test_command(char *line, int command)
{
	if (ft_strcmp(line, "##start") == 0)
		return (1);
	if (ft_strcmp(line, "##end") == 0)
		return (2);
	return (command);
}

static int	print_com(char *line)
{
	ft_putendl(line);
	return (0);
}

static int	ft_error_l(char *line, int linker, int error)
{
	if (line && line[0] == 'L')
		error = (linker) ? 2 : 1;
	ft_strdel(&line);
	return (error);
}

int			ft_lemin_lexer(t_env *e)
{
	char	*line;
	int		linker;
	int		error;
	int		com;
	int		command;

	linker = error = command = 0;
	while (!error && get_next_line(0, &line) > 0 && (com = 1) && line[0] != 'L')
	{
		command = test_command(line, command);
		if (test_command(line, 0))
			com = print_com(line);
		else if (line && line[0] == '#')
			com = 0;
		else
			linker = ft_ret_link_value(line, linker);
		if (com && !linker)
			error = ft_lst_room(e, line, &command);
		else if (com)
			error = ft_parse(e, line);
		ft_strdel(&line);
	}
	error = ft_error_l(line, linker, error);
	return (error);
}
