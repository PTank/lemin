/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_graph.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 15:04:10 by akazian           #+#    #+#             */
/*   Updated: 2014/03/23 19:56:49 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>
#include <libft.h>
#include <stdlib.h>

t_graph	*new_graph_elem(void)
{
	t_graph	*new;

	if ((new = (t_graph *)ft_memalloc(sizeof(t_graph))) == NULL)
		return (NULL);
	new->name = NULL;
	new->links = NULL;
	new->y = 0;
	new->x = 0;
	new->next = NULL;
	new->here = 0;
	return (new);
}

void	list_graph_elem(t_graph **g, int y, int x, char *name)
{
	t_graph	*tmp;

	if ((tmp = new_graph_elem()))
	{
		tmp->y = y;
		tmp->x = x;
		tmp->next = *g;
		tmp->name = name;
		*g = tmp;
	}
}

t_graph	*search_graph_elem(t_graph *g, char *name)
{
	t_graph	*tmp;

	if (g)
	{
		tmp = g;
		while (tmp && tmp->name && ft_strcmp(tmp->name, name))
			tmp = tmp->next;
		if (tmp && tmp->name && ft_strcmp(tmp->name, name) == 0)
			return (tmp);
	}
	return (NULL);
}

void	del_graph(t_graph *g)
{
	t_graph	*tmp;
	t_graph	*next;

	tmp = g;
	while (tmp)
	{
		next = tmp;
		if (tmp->name)
			free(tmp->name);
		if (tmp->links)
			free(tmp->links);
		tmp = tmp->next;
		free(next);
	}
}
